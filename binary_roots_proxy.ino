/**

  Reads commands like ping and status from Master, and
  responds to them.

  Use writeNodeDebug(0, 3, "got envelope"); to send debug info
  to Master.

  Developed using TMRh20's implementation of RF24 library.
  https://github.com/TMRh20/RF24
*/

#include <RF24_config.h>
#include <nRF24L01.h>
#include <RF24.h> // TMRh20

#include <SoftwareSerial.h>



//
// > Nodes definition starts ----------------------------------------------
//
#define HEADER_END         0x80
#define NODE_END           0x00

#define NODE_PING          0x01
#define NODE_PONG          0x02
#define NODE_PACKET_NUM    0x03

#define NODE_STATUS        0x05
#define NODE_MILLIS        0x06
#define NODE_STATUS_CHANGE 0x07
#define NODE_LED           0x08
#define NODE_SWITCH        0x09
#define NODE_ENVELOPE      0x11
#define NODE_TO            0x12
#define NODE_FROM          0x13
#define NODE_DATA          0x14
//
// < Nodes definition ends
//

#define NODE_DEBUG         0x20
#define NODE_LEVEL         0x21
#define NODE_STEP          0x22
#define NODE_TEXT          0x23
//
// < Nodes definition ends
//


//#define PIPE_BUFFER_DEBUG

//
// > PipeBuffer class starts ----------------------------------------------
//
/*
  The state of pointers is same for the empty and full buffers. Check if
  it can be resolved somehow (for ex., using an additional boolean flag
  of fullness. One byte is lost otherwise.
*/

class PipeBuffer {
  public:
    PipeBuffer(uint8_t byteArray[], uint8_t len);
    uint8_t   size();
    void      writeByte(uint8_t b);
    void      writeArray(uint8_t byteArray[], uint8_t startP, uint8_t endP);
    void      writeArray(uint8_t byteArray[], uint8_t length);
    uint8_t   readByte();
    void      readArray(uint8_t byteArray[], uint8_t start, uint8_t length);
    void      readArray(uint8_t byteArray[], uint8_t length);
    void      saveState();
    void      restoreState();
    void      pushBack();
#ifdef PIPE_BUFFER_DEBUG
    void      printBuffState();
#endif

  private:
    uint8_t*  buff;
    uint8_t   len;
    uint8_t   s = 0; // Start. Points to the last readed byte
    uint8_t   e = 0; // End. Points to the last written (unread) byte
    uint8_t   sTemp = s; // Start and end pointers copy. Used for saving/restoring state.
    uint8_t   eTemp = e;
};


PipeBuffer::PipeBuffer(uint8_t byteArray[], uint8_t length) {
  buff = byteArray;
  len = length;
}


uint8_t PipeBuffer::size() {
  if (s <= e) {
    return e - s;
  } else {
    return e + len - s;
  }
}

#ifdef PIPE_BUFFER_DEBUG
void PipeBuffer::printBuffState() {
  Serial.println();
  Serial.print("a="); Serial.println(size());

  for (uint8_t i = 0; i < len; ++i) {
    if (s == i) {
      Serial.print('s');
    }
    if (e == i) {
      Serial.print('e');
    }
    Serial.print(i);
    Serial.print(": ");
    Serial.println(buff[i]);
  }
}
#endif


void PipeBuffer::writeByte(uint8_t b) {

  if (++e == len) {
    e = 0;
  }

  buff[e] = b;

#ifdef PIPE_BUFFER_DEBUG
  printBuffState();
  Serial.println();
#endif
}

/**
  Writes an array to the buffer, starting from start pointer and until end poiner.
  Does NOT include byte at the end pointer.
*/
void PipeBuffer::writeArray(uint8_t byteArray[], uint8_t start, uint8_t end) {

  for (; start < end; ++start) {
    writeByte(byteArray[start]);
  }
}


void PipeBuffer::writeArray(uint8_t byteArray[], uint8_t length) {

  writeArray(byteArray, 0, length);
}


uint8_t PipeBuffer::readByte() {
  if (++s == len) {
    s = 0;
  }
  return buff[s];
}


void PipeBuffer::readArray(uint8_t byteArray[], uint8_t start, uint8_t lenToRead) {

  uint8_t i = start;
  while (lenToRead-- > 0) {
    byteArray[i++] = readByte();
  }
}


void PipeBuffer::readArray(uint8_t byteArray[], uint8_t lenToRead) {

  readArray(byteArray, 0, lenToRead);
}


void PipeBuffer::saveState() {
  sTemp = s;
  eTemp = e;
}


void PipeBuffer::restoreState() {
  s = sTemp;
  e = eTemp;
}


void PipeBuffer::pushBack() {
  if (s == 0) {
    s = len;
  }
  s--;
}

//
// < PipeBuffer class ends ------------------------------------------------
//


// v. 2018.11.19
// > RootReader class starts ----------------------------------------------
//
class RootReader {
  public:
    RootReader(PipeBuffer& inBuffer);
    boolean     isUint32Available();
    uint32_t    readUint32();
    int32_t     readInt31();
    boolean     isUint7Available();
    uint8_t     readUint7();
    boolean     skipRoot();
    boolean     skipToHeader();
    boolean     isHeaderAvailable();
    int16_t     readHeader();
    uint8_t     size();
  private:
    PipeBuffer& inBuf;
    uint16_t    readType(byte firstByte);
    uint8_t     skipDeep = 0;
};

RootReader::RootReader(PipeBuffer& inBuffer) : inBuf(inBuffer) {
  inBuf = inBuffer;
}

uint16_t RootReader::readType(byte firstByte) {
  uint16_t result = firstByte & 0x3f;
  if (!(firstByte & 0x40)) {
    return result;
  }
  uint8_t shift = 6;
  do {
    firstByte = inBuf.readByte();
    uint16_t temp = firstByte & 0x3f;
    result = result + (temp << shift);
    shift += 6;
  } while (firstByte & 0x40);
  return result;
}

/**
  Returns header, found in the input buffer. Returns -1 if there is no data or
  no headers are available.
*/
int16_t RootReader::readHeader() {

  uint8_t octet;
  // Skip data until header is found
  while (inBuf.size()) {
    if ((octet = inBuf.readByte()) >= 0x80) {
      return readType(octet);
    }
  }
  return -1;
}


boolean RootReader::isUint7Available() {

  if (!inBuf.size()) {
    return false;
  }

  boolean result = readUint7() < 0x80;
  inBuf.pushBack();
  return result;
}

/**
  This method is used for speed up purpose, instead of resource-cost 'readUint32'.
*/
uint8_t RootReader::readUint7() {

  return inBuf.readByte();
}


boolean RootReader::isUint32Available() {

  uint8_t octet;
  boolean hasLowByte = false;
  boolean hasHighByte = false;

  inBuf.saveState();
  while (inBuf.size() && (octet = inBuf.readByte()) < 0x80) {
    hasLowByte = true;
  }
  while (inBuf.size() && (octet = inBuf.readByte()) >= 0x80) {
    hasHighByte = true;
  }
  inBuf.restoreState();

  return hasLowByte && hasHighByte;
}


int32_t RootReader::readInt31() {

  uint32_t result = 0x00;
  uint8_t  shift = 0;
  uint32_t octet;

  while ((octet = inBuf.readByte()) < 0x80) {
    octet = octet << shift;
    result |= octet;
    shift += 7;
  }
  inBuf.pushBack();
  result = (result >> 1) ^ -(result & 1);
  return result;
}


uint32_t RootReader::readUint32() {

  uint32_t result = 0x00;
  uint8_t  shift = 0;
  uint32_t octet;

  while ((octet = inBuf.readByte()) < 0x80) {
    octet = octet << shift;
    result |= octet;
    shift += 7;
  }
  inBuf.pushBack();

  return result;
}


boolean RootReader::skipRoot() {

  uint8_t octet;

  while (inBuf.size()) {
    octet = inBuf.readByte();
    if (octet == 0x80) {
      if (skipDeep == 0) {
        return true;
      } else {
        skipDeep--;
      }
    } else if (octet > 0x80) {
      skipDeep++;
    }
  }
  return false;
}


/**
  Skips current stream data to header (either open or close).
  Returns true if the header start found, false otherwise.
*/
boolean RootReader::skipToHeader() {

  uint8_t octet;

  do {
  } while (inBuf.size() && (octet = inBuf.readByte()) < 0x80);
  if (octet >= 0x80) {
    inBuf.pushBack();
    return true;
  } else {
    return false;
  }
}

boolean RootReader::isHeaderAvailable() {

  if (!skipToHeader()) {
    return false;
  }
  return true;
}

uint8_t RootReader::size() {

  return inBuf.size();
}
//
// < RootReader class ends ----------------------------------------------
//


//
// > RootWriter class starts --------------------------------------------
//
class RootWriter {
  public:
    RootWriter(PipeBuffer& outBuffer);
    void writeHeader(uint8_t nodeType);
    void writeEnd();
    void writeUint32(uint32_t data);
    void writeInt31(int32_t data);
    void writeUint7(uint8_t data);
    void writeTextAscii(const char text[]);
    void writeSimpleNode(uint8_t nodeType);
  private:
    PipeBuffer& outBuf;
    void writeByte(uint8_t byte);
};


RootWriter::RootWriter(PipeBuffer& outBuffer) : outBuf(outBuffer) {
  outBuf = outBuffer;
}

void RootWriter::writeByte(uint8_t byte) {
  outBuf.writeByte(byte);
}

void RootWriter::writeHeader(uint8_t type) {
  writeByte(0x80 | (type & 0x3f));
}

void RootWriter::writeEnd() {
  writeByte(HEADER_END);
}

void RootWriter::writeUint7(uint8_t data) {

  writeByte(data & 0x7f);
}

void RootWriter::writeTextAscii(const char text[]) {

  for (uint8_t i = 0; i < sizeof(text) - 1; i++) {
    writeUint7(text[i]);
  }
}

void RootWriter::writeUint32(uint32_t data) {
  do {
    uint8_t toSend = data & 0x7f;
    writeByte(toSend);
    data >>= 7;
  } while (data != 0);
}

void RootWriter::writeInt31(int32_t data) {
  
  data = (data >> 31) ^ (data << 1);
  writeUint32(data);
}

void RootWriter::writeSimpleNode(uint8_t nodeType) {
  writeHeader(nodeType);
  writeEnd();
}

//
// < RootWriter class ends --------------------------------------------------
//



//
// > StackArray class starts ------------------------------------------------
//
// the definition of the stack class.
template<typename T>
class StackArray {
  public:
    // init the stack (constructor).
    StackArray (T array[], int length);

    // push an item to the stack.
    void push (const T i);

    // pop an item from the stack.
    T pop ();

    // get an item from the stack.
    T peek () const;

    // check if the stack is empty.
    bool isEmpty () const;

    // get the number of items in the stack.
    int count () const;

    // check if the stack is full.
    bool isFull () const;

  private:
    int top;                // the top index of the stack.
    T * contents;           // the array of the stack.
    int size;               // the size of the stack.
};

// init the stack (constructor).
template<typename T>
StackArray<T>::StackArray (T array[], int length) {

  top = 0;              // set the initial top index of the stack.
  contents = array;
  size = length;
}


// push an item to the stack.
template<typename T>
void StackArray<T>::push (const T i) {
  // store the item to the array.
  contents[top++] = i;
  if (top > size) {
    top--;
  }
}

// pop an item from the stack.
template<typename T>
T StackArray<T>::pop () {
  // fetch the top item from the array.
  if (top == 0) {
    return contents[0];
  }
  return contents[--top];
}

// get an item from the stack.
template<typename T>
T StackArray<T>::peek () const {
  // get the top item from the array.
  return contents[top - 1];
}

// check if the stack is empty.
template<typename T>
bool StackArray<T>::isEmpty () const {
  return top == 0;
}

// check if the stack is full.
template<typename T>
bool StackArray<T>::isFull () const {
  return top >= size;
}

// get the number of items in the stack.
template<typename T>
int StackArray<T>::count () const {
  return top;
}
//
// < StackArray class ends --------------------------------------------------
//













//
// Program part starts
//


typedef void (*ProcessorFunc)();


#define RADIO_XCS           8
#define RADIO_CE            7
#define RADIO_CHANNEL       121
#define RADIO_PAYLOAD_SIZE  32
#define AUTO_ACK_ENABLED    false

#define MASTER_ADDR         0x63C1FA6300LL // This device for Master
#define SLAVE_BASE_ADDR     0x63C1FA6300LL

uint64_t currentSlaveAddr = SLAVE_BASE_ADDR + 2;

#define SERIAL_SPEED        115200

RF24 radio(RADIO_CE, RADIO_XCS);

uint8_t addr = 0;


// Input master buffers
#define       IN_MASTER_BUFF_LEN         32

uint8_t       inMasterArray[IN_MASTER_BUFF_LEN];
PipeBuffer    inMasterPipe = PipeBuffer(inMasterArray, IN_MASTER_BUFF_LEN);

RootReader    rrM = RootReader(inMasterPipe);


// Output master buffers
#define       OUT_MASTER_BUFF_LEN         32
#define       OUT_MASTER_FLUSH_THRESHOLD  OUT_MASTER_BUFF_LEN / 2

uint8_t       outMasterPipeArray[OUT_MASTER_BUFF_LEN + 1]; // An array for out master pipe
PipeBuffer    outMasterPipe = PipeBuffer(outMasterPipeArray, OUT_MASTER_BUFF_LEN + 1);
RootWriter    rwM = RootWriter(outMasterPipe);

uint8_t       outMasterBuffArray[OUT_MASTER_BUFF_LEN]; // A buffer array used to send an info out to master


// Input leafs buffers
uint8_t       inLeafArray[RADIO_PAYLOAD_SIZE];


// Output leafs buffers
#define       OUT_LEAF_BUFF_LEN          RADIO_PAYLOAD_SIZE * 2 + 1
#define       OUT_LEAF_FLUSH_THRESHOLD   RADIO_PAYLOAD_SIZE

uint8_t       outLeafPipeArray[RADIO_PAYLOAD_SIZE + 1];
PipeBuffer    outLeafPipe = PipeBuffer(outLeafPipeArray, RADIO_PAYLOAD_SIZE + 1);

uint8_t       outLeafBuffArray[RADIO_PAYLOAD_SIZE]; // A buffer array used to send an info out to leaf



// Processors data

// The lengh of the stack of Processor functions
#define NODE_READERS_STACK_DEEP    8

ProcessorFunc stackArr[NODE_READERS_STACK_DEEP];
ProcessorFunc nextProcessor = 0;

StackArray <ProcessorFunc> stack = StackArray<ProcessorFunc>(stackArr, NODE_READERS_STACK_DEEP);


// Other
uint32_t      loopTime = 0;
bool          outFlushRequested = false;




// Write methods ---------------------------------------
/*
  uint8_t oArray[] = {1};

  void transmitOctet(byte octet){

  oArray[0] = octet;
  radio.openWritingPipe(currentSlaveAddr);
  radio.stopListening();
  radio.write(oArray, 1, 0);
  radio.startListening();
  }
*/

void writeNodePong() {
  rwM.writeSimpleNode(NODE_PONG);
}


void writeNodeMillis() {
  rwM.writeHeader(NODE_MILLIS);
  uint32_t timeMillis = millis();
  rwM.writeUint32(timeMillis);
  rwM.writeEnd();
}

void writeNodeLevel(uint8_t level) {
  rwM.writeHeader(NODE_LEVEL);
  rwM.writeUint7(level);
  rwM.writeEnd();
}

void writeNodeStep(uint8_t step) {
  rwM.writeHeader(NODE_STEP);
  rwM.writeUint7(step);
  rwM.writeEnd();
}

void writeNodeText(char text[]) {
  rwM.writeTextAscii(text);
}

void writeNodeDebug(uint8_t level, uint8_t step) {
  rwM.writeHeader(NODE_DEBUG);
  writeNodeLevel(level);
  writeNodeStep(step);
  rwM.writeEnd();
}

void writeNodeDebug(uint8_t level, uint8_t step, char text[]) {
  rwM.writeHeader(NODE_DEBUG);
  writeNodeLevel(level);
  writeNodeStep(step);
  writeNodeText(text);
  rwM.writeEnd();
}


void writeNodeLed() {
  rwM.writeHeader(NODE_LED);
  if (digitalRead(LED_BUILTIN)) {
    rwM.writeUint7(HIGH);
  } else {
    rwM.writeUint7(LOW);
  }
  rwM.writeEnd();
}


void writeNodeStatus() {
  rwM.writeHeader(NODE_STATUS);
  writeNodeLed();
  writeNodeMillis();
  rwM.writeEnd();
}

/**

  Read processors.

  Each @Processor should set the value of 'nextProcessor' (global variable):
  - set it to istelf, if @Processor would like to proceed on the next cycle or
  when new data arrives;
  - to another @Processor, if there is a known header of nested node;
  - to 'readUnknownNode' if there is an unknown header, to skip nested node;
  - to 'readNodeUntilEnd' if this processor has finished but waits for the end of it's node;
  - to '0' (zero), if @Processor has finished and already got the end of it's node.

  Should be avoided not to set the value of 'nextProcessor' at all.

*/

//@Processor
void readNodeUntilEnd() {
  if (rrM.skipRoot()) {
    nextProcessor = 0;
  } else {
    nextProcessor = readNodeUntilEnd;
  }
}


//@Processor
void readUnknownNode() {
  nextProcessor = readNodeUntilEnd;
}


//@Processor
// Transfer data to addressed node
uint8_t transferDeep = 0;
void transferNodeData() {

  uint8_t octet;

  while (inMasterPipe.size()) {
    octet = inMasterPipe.readByte();
    if (octet == 0x80) {
      if (transferDeep == 0) {
        nextProcessor = 0;
        return;
      } else {
        transferDeep--;
      }
    } else if (octet > 0x80) {
      transferDeep++;
    }
    outLeafPipe.writeByte(octet);
  }
  nextProcessor = transferNodeData;
  return;
}


//@Processor
void readNodePing() {
  writeNodePong();
  nextProcessor = readNodeUntilEnd;
}


//@Processor
void readNodeTo() {
  // FIXME currentSlaveAddr = SLAVE_BASE_ADDR + rrM.readUint7();
  nextProcessor = readNodeUntilEnd;
}


//@Processor
void readNodeEnvelope() {

  if (rrM.isHeaderAvailable()) {
    uint16_t type = rrM.readHeader();
    switch (type) {
      case NODE_TO:
        nextProcessor = readNodeTo;
        break;
      case NODE_DATA:
        nextProcessor = transferNodeData;
        break;
      case NODE_END:
        nextProcessor = 0;
        break;
      default:
        nextProcessor = readUnknownNode;
    }
  } else {
    nextProcessor = 0;
  }
}


//@Processor
void readNodeStatus() {
  writeNodeStatus();
  nextProcessor = readNodeUntilEnd;
}


//@Processor
void readMaster() {

  if (rrM.isHeaderAvailable()) {
    uint16_t type = rrM.readHeader();
    switch (type) {
      case NODE_PING:
        nextProcessor = readNodePing;
        break;
      case NODE_ENVELOPE:
        nextProcessor = readNodeEnvelope;
        break;
      case NODE_STATUS:
        nextProcessor = readNodeStatus;
        break;
      case NODE_END:
        nextProcessor = 0;
        break;
      default:
        nextProcessor = readUnknownNode;
    }
  } else {
    nextProcessor = 0;
  }
}


void processMasterData() {

  if (!inMasterPipe.size()) {
    return;
  }

  ProcessorFunc processor;
  if (stack.isEmpty()) {
    processor = readMaster;
  } else {
    processor = stack.peek();
  }

  processor();
  if (nextProcessor == processor) { // the current one is going to proceed, do nothing
    return;
  } else if (nextProcessor == readNodeUntilEnd) { // the current node has finished,
    // and INSTEAD of it proceed with dummy reader until node ends
    stack.pop();
    stack.push(nextProcessor);
  } else if (nextProcessor == 0) { // the current one is done, remove it from stack
    stack.pop();
  } else {
    stack.push(nextProcessor); // the other processor is returned, proceed with it
  }
}


void setupRadio() {

  radio.begin();

  radio.enableDynamicPayloads();
  radio.setChannel(RADIO_CHANNEL);
  radio.setAutoAck(AUTO_ACK_ENABLED);

  radio.openWritingPipe(currentSlaveAddr);
  radio.openReadingPipe(1, MASTER_ADDR);

  radio.enableDynamicAck();
  radio.startListening();
  //  radio.printDetails();
}


SoftwareSerial sSerial(10, 9); // RX, TX

void setupSSerial() {
  sSerial.begin(115200);
  sSerial.println("Software serial started");
}

void setup() {

  Serial.begin(SERIAL_SPEED);
  setupRadio();
  setupSSerial();
}


void sendBuffToMaster(byte* data, uint8_t len) {

  Serial.write(data, len);
  sSerial.print("ir: ");
  for (uint8_t i = 0; i < len; i++) {
    sSerial.print(data[i], HEX);
    sSerial.print(' ');
  }
  sSerial.println();
}


void readInMasterData() {
  while (Serial.available()) {
    int inMasterByte = Serial.read();
    inMasterPipe.writeByte(inMasterByte);
    sSerial.print("im: ");
    sSerial.println(inMasterByte, HEX);
  }
}


void flushOutMasterBuffer() {

  uint8_t toSend = outMasterPipe.size();
  outMasterPipe.readArray(outMasterBuffArray, toSend);
  sendBuffToMaster(outMasterBuffArray, toSend);
}


void flushResponseToMaster() {

  if (outMasterPipe.size() == 0) {
    return;
  }

  if (outFlushRequested || stack.isEmpty() || outMasterPipe.size() >= OUT_MASTER_FLUSH_THRESHOLD) {
    flushOutMasterBuffer();
  }

  // TODO Add checking for the timeout also
}


void radioToMaster() {

  // Check Leafs stream
  if (radio.available()) {
    uint8_t len = radio.getDynamicPayloadSize();
    if (len > 0) { // No need to read zero packet
      radio.read(&inLeafArray, len);
      sendBuffToMaster(inLeafArray, len);
    }
  }
}


void transmitLeafPacket(byte byteArray[], uint8_t len) {

  radio.openWritingPipe(currentSlaveAddr);
  radio.stopListening();
  radio.write(byteArray, len, 0);
  radio.startListening();
}



void flushOutLeafBuffer() {

  uint8_t toSend = outLeafPipe.size();
  outLeafPipe.readArray(outLeafBuffArray, toSend);
  transmitLeafPacket(outLeafBuffArray, toSend);
}


void flushLeafData() {

  if (outLeafPipe.size() == 0) {
    return;
  }

  if (outFlushRequested || stack.isEmpty() || outLeafPipe.size() >= OUT_LEAF_FLUSH_THRESHOLD) {
    flushOutLeafBuffer();
  }

  // TODO Add checking for the timeout also
}


void loop() {

  loopTime = millis();
  readInMasterData();
  processMasterData();
  flushResponseToMaster();
  flushLeafData();
  radioToMaster();
}
